<?php

namespace App\Entity;

use DateTimeZone;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FavouriteRepository")
 */
class Favourite
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reddit_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $user;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $registeredAt;

    public function __construct()
    {
        $this->registeredAt = new \DateTime("now", new DateTimeZone('Asia/Bishkek'));
    }

    public function getId()
    {
        return $this->id;
    }

    public function getRedditName()
    {
        return $this->reddit_name;
    }

    public function setRedditName(string $reddit_name)
    {
        $this->reddit_name = $reddit_name;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(string $user)
    {
        $this->user = $user;

        return $this;
    }
    /**
     * @return \DateTime
     */
    public function getRegisteredAt(): ?\DateTime
    {
        return $this->registeredAt;
    }

    /**
     * @param \DateTime $registeredAt
     */
    public function setRegisteredAt(\DateTime $registeredAt): void
    {
        $this->registeredAt = $registeredAt;
    }
}
