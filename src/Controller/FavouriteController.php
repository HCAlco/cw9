<?php

namespace App\Controller;

use App\Entity\Favourite;
use App\Entity\User;
use App\Form\LoginType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\FavouriteRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/favourite")
 */
class FavouriteController extends Controller
{
    /**
     * @Route("/add", name="app_favourite_add")
     * @param Request $request
     * @param ApiContext $apiContext
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws ApiException
     */
    public function addFavouriteAction(
        Request $request,
        ApiContext $apiContext
    )
    {
        $name = $request->get('name');
        $user = $this->getUser();

        $dql_result = $this->getDoctrine()->getRepository('App:Favourite')->findByNameAndUser($name, $user);
        if(!empty($dql_result)){
            return new JsonResponse(true);
        }else{
            $favourite = new Favourite();
            $favourite->setRedditName($request->get('name'));
            $favourite->setUser($this->getUser()->getId());
            $em = $this->getDoctrine()->getManager();
            $em->persist($favourite);
            $em->flush();

            return new JsonResponse(false);
        }
    }

    /**
     * @Route("/my", name="app_favourite_view_my_favourites")
     * @param ApiContext $apiContext
     * @param FavouriteRepository $favouriteRepository
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws ApiException
     */
    public function viewMyFavouritesAction(
        ApiContext $apiContext,
        FavouriteRepository $favouriteRepository,
        Request $request
    )
    {
        $user_id = $this->getUser()->getId();
        $offset = 0;
        if (!empty($request->get('page'))){
            $offset =  ($request->get('page')-1) * 2;
        }
        $posts = $favouriteRepository->findMyFavoritesWithOffset($user_id, $offset);
        $qty = $favouriteRepository->findLikedCount($user_id);

        $pages = ceil($qty[0]['caunto']/2);

        $id = '';
        foreach($posts as $key=>$post){
            $id .= $post->getRedditName().',';
        }
        $result = $apiContext->getPostByName(['id' => $id]);
        dump($posts);
        return $this->render('base/favourites.html.twig', [
            'posts' => $result['data']['children'],
            'pages' => $pages
        ]);
    }

    /**
     * @Route("/society", name="app_favourite_view_society_favourites")
     * @param ApiContext $apiContext
     * @param FavouriteRepository $favouriteRepository
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws ApiException
     */
    public function viewSocietyFavouritesAction(
        ApiContext $apiContext,
        FavouriteRepository $favouriteRepository,
        Request $request
    )
    {
        $offset = 0;
        if (!empty($request->get('page'))){
            $offset =  ($request->get('page')-1) * 2;
        }
        $all_posts = $favouriteRepository->findSocietyFavorites();
        $posts_per_page = $favouriteRepository->findFavouritesForPagination($offset);
        $qty = count($all_posts);
        $pages = ceil($qty/2);
        $id = '';
        foreach($posts_per_page as $key=>$post){
            $id .= $post['reddit_name'].',';
        }
        $result = $apiContext->getPostByName(['id' => $id]);

        return $this->render('base/society_favourites.html.twig', [
            'posts' => $result['data']['children'],
            'pages' => $pages,
        ]);
    }
}
