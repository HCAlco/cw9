<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\LoginType;
use App\Model\Api\ApiContext;
use App\Model\User\UserHandler;
use App\Repository\FavouriteRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class BasicController extends Controller
{
    /**
     * @Route("/", name="app_homepage")
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Model\Api\ApiException
     */
    public function indexAction(Request $request , ApiContext $apiContext)
    {
        $result= null;
        $form = $this->createForm('App\Form\FilterType');
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $result = $apiContext->getInfoFromReddit(['q'=> $data['word'], 'sort' => $data['sort'], 'limit' => $data['qty'], 'type' => 'link']);
        }
        return $this->render('base/index.html.twig', [
            'form' => $form->createView(),
            'data' => $result['data']['children']
        ]);
    }

    /**
     * @Route("/post/{name}")
     * @param string $name
     * @param ApiContext $apiContext
     * @param FavouriteRepository $favouriteRepository
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Model\Api\ApiException
     */
    public function postAction(string $name , ApiContext $apiContext, favouriteRepository $favouriteRepository)
    {
        $result = $apiContext->getPostByName(['id' => $name]);
        $count = $favouriteRepository->findLikedCount($name);

        return $this->render('base/post.html.twig', [
            'data' => $result['data']['children'][0]['data'],
            'count' => $count[0]
        ]);
    }
}
