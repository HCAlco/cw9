<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\LoginType;
use App\Model\Api\ApiContext;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserController extends Controller
{
    /**
     * @Route("/register", name="app_register")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('App\Form\RegisterType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('app_login', array());
        }

        return $this->render('base/register.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/login", name="app_login")
     * @param Request $request
     * @param UserRepository $userRepository
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request, UserRepository $userRepository, UserHandler $userHandler)
    {
        $error = '';
        $user = new User();
        $form = $this->createForm(LoginType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $dql_result = $userRepository->findOneByEmailAndPassword($user->getEmail(), $user->getPassword());
            if(!empty($dql_result)){
                $userHandler->putUserInSession($dql_result);
                return $this->redirectToRoute('app_homepage');
            }
        }
        return $this->render('base/login.html.twig', array(
            'form' => $form->createView(),
            'error' => $error
        ));
    }
}
