<?php

namespace App\Model\Api;


class ApiContext extends AbstractApiContext
{
    /**
     * @return mixed
     * @throws ApiException
     */
    public function getInfoFromReddit($data)
    {
        return $this->makeQuery('https://www.reddit.com/r/picture/search.json', self::METHOD_GET, $data);
    }

    /**
     * @param $data
     * @return mixed
     * @throws ApiException
     */
    public function getPostByName($data)
    {
        return $this->makeQuery('https://www.reddit.com/api/info.json', self::METHOD_GET, $data);
    }
}
