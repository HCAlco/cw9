<?php

namespace App\DataFixtures;

use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    private $userHandler;

    public const USER_ONE = 'user_one';
    public const USER_TWO = 'user_two';
    public const USER_THREE = 'user_three';


    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    /**
     * @param ObjectManager $manager
     * @throws \App\Model\Api\ApiException
     */
    public function load(ObjectManager $manager)
    {

        $user = $this->userHandler->createNewUser([
            'email' => 'user1',
            'password' => '1234',
        ], false);

        $user1 = $this->userHandler->createNewUser([
            'email' => 'user2',
            'password' => '1234',
        ], false);

        $user2 = $this->userHandler->createNewUser([
            'email' => 'user3',
            'password' => '1234',
        ], false);

        $manager->persist($user);
        $manager->persist($user1);
        $manager->persist($user2);
        $manager->flush();

        $this->addReference(self::USER_ONE, $user);
        $this->addReference(self::USER_TWO, $user1);
        $this->addReference(self::USER_THREE, $user2);
    }
}
