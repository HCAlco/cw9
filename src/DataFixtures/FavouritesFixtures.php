<?php

namespace App\DataFixtures;

use App\Entity\Favourite;
use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class FavouritesFixtures extends Fixture implements DependentFixtureInterface
{
    private $userHandler;


    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    /**
     * @param ObjectManager $manager
     * @throws \App\Model\Api\ApiException
     */
    public function load(ObjectManager $manager)
    {
        $user1 = $this->getReference(UserFixtures::USER_ONE);
        $user2 = $this->getReference(UserFixtures::USER_TWO);
        $user3 = $this->getReference(UserFixtures::USER_THREE);

        $posts = ['t3_89nnsp', 't3_8k3o4v', 't3_8wpekh', 't3_8ni7i0', 't3_8unv2t', 't3_8vtwwx'];

        for($i = 0; $i < count($posts); $i++){
            $favourites[$i] = new Favourite();
            $favourites[$i]->setRedditName($posts[$i]);
            $favourites[$i]->setUser($user1->getId());
            $manager->persist($favourites[$i]);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
        );
    }
}
