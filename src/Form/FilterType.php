<?php
/**
 * Created by PhpStorm.
 * User: aibek
 * Date: 13.06.18
 * Time: 15:50
 */

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('word', TextType::class, ['label' => 'Поисковое слово'])
            ->add('qty', ChoiceType::class, ['label' => 'Количество',
                'choices' => [
                    '8шт' => '8',
                    '12шт' => '12',
                    '16шт' => '16',
                    '50шт' => '50',
                    '100шт' => '100',
                ],

            ])
            ->add('sort', ChoiceType::class,['label' => 'Сортировка',
                'choices' => [
                    'По релевантности' => 'relevance',
                    'Горячие' => 'hot',
                    'В топе' => 'top',
                    'Новые' => 'new',
                    'Комментируемые' => 'comments'
                ],

            ])
            ->add('save', SubmitType::class, ['label' => 'Поиск',
                'attr' => array('class' => 'btn btn-primary')]);;

    }
}