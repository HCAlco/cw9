<?php

namespace App\Repository;

use App\Entity\Favourite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Favourite|null find($id, $lockMode = null, $lockVersion = null)
 * @method Favourite|null findOneBy(array $criteria, array $orderBy = null)
 * @method Favourite[]    findAll()
 * @method Favourite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FavouriteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Favourite::class);
    }


    public function findByNameAndUser($name, $user)
    {
        return $this->createQueryBuilder('f')
            ->where('f.reddit_name = :name')
            ->andWhere('f.user = :user')
            ->setParameter('name', $name)
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult()
        ;
    }
    public function findByUser($user)
    {
        return $this->createQueryBuilder('f')
            ->where('f.user = :user')
            ->setParameter('user', $user)
            ->orderBy('f.registeredAt', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findSocietyFavorites()
    {
        return $this->createQueryBuilder('a')
            ->select('a.reddit_name, COUNT(a.reddit_name) AS b')
            ->groupBy('a.reddit_name')
            ->orderBy('b', "desc")
            ->getQuery()
            ->getResult();
    }

    public function findMyFavoritesWithOffset($user, $offset, $limit = 2)
    {
        return $this->createQueryBuilder('a')
            ->where('a.user = :user')
            ->setParameter('user', $user)
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('a.registeredAt', "desc")
            ->getQuery()
            ->getResult();
    }

    public function findLikedCount($data)
    {
        return $this->createQueryBuilder('f')
            ->select('COUNT(f.id) AS caunto')
            ->where('f.reddit_name = :data')
            ->orWhere('f.user = :data')
            ->setParameter('data', $data)
            ->getQuery()
            ->getResult()
            ;
    }


    public function findFavouritesForPagination($offset, $limit = 2)
    {
        return $this->createQueryBuilder('a')
            ->select('a.reddit_name, COUNT(a.reddit_name) AS b')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->groupBy('a.reddit_name')
            ->orderBy('b', "desc")
            ->getQuery()
            ->getResult();

    }


    /*
    public function findOneBySomeField($value): ?Favourite
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
