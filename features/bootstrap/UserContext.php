<?php

/**
 * Defines application features from the specific context.
 */
class UserContext extends AttractorContext
{
//    /**
//     * @When /^я вижу слово "([^"]*)" где\-то на странице$/
//     */
//    public function яВижуСловоГдеТоНаСтранице($arg1)
//    {
//        $this->assertPageContainsText($arg1);
//    }

    /**
     * @When /^я вижу слово "([^"]*)" где\-то на странице$/
     */
    public function яВижуСловоГдеТоНаСтранице($arg1)
    {
        $this->assertPageContainsText($arg1);
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function яНахожусьНаГлавнойСтранице()
    {
        $this->visit($this->getContainer()->get('router')->generate('app_homepage'));
    }

    /**
     * @When /^я нахожусь на странице регистрации пользователей$/
     */
    public function яНахожусьНаСтраницеРегистрацииПользователей()
    {
        $this->visit($this->getContainer()->get('router')->generate('app_register'));
    }

    /**
     * @When /^я нахожусь на странице авторизации пользователей$/
     */
    public function яНахожусьНаСтраницеАвторизацииПользователей()
    {
        $this->visit($this->getContainer()->get('router')->generate('app_login'));
    }

    /**
     * @When /^я регистрируюсь со следующими данными email:"([^"]*)" password: "([^"]*)"$/
     * @param $email
     * @param $passport
     * @param $password
     * @param $role
     */
    public function яРегистрируюсьСоСледующимиДанными($email, $password)
    {
        $this->fillField('register_email', $email);
        $this->fillField('register_password', $password);
        $this->pressButton('register_save');
    }

    /**
     * @When /^я авторизуюсь со следующими данными email:"([^"]*)" password: "([^"]*)"$/
     */
    public function яАвторизуюсьСоСледующимиДанными($email, $password)
    {
        $this->fillField('login_email', $email);
        $this->fillField('login_password', $password);
        $this->pressButton('login_save');
    }

    /**
     * @When /^я ищу посты со следующими данными word:"([^"]*)" qty: "([^"]*)" sort: "([^"]*)"$/
     */
    public function яИщуПостыСоСледующимиДанными($word, $qty, $sort)
    {
        $this->fillField('filter_word', $word);
        $this->selectOption('filter_qty', $qty);
        $this->selectOption('filter_sort', $sort);
        $this->pressButton('filter_save');
    }

    /**
     * @When /^я жму на ссылку link:"([^"]*)"$/
     */
    public function яЖмуНаСсылку($link)
    {
        $this->clickLink($link);
    }

    /**
     * @When /^я жму на кнопку button:"([^"]*)"$/
     */
    public function яЖмуНаКнопку($button)
    {
        $this->pressButton($button);
    }

}

